# Frontend Mentor - Sunnyside agency landing page

![Design preview for the Sunnyside agency landing page coding challenge](./design/desktop-preview.jpg)

## Links

- Live site: <https://fem-sunnyside-landing-page.vercel.app/>
- Solution page: <https://www.frontendmentor.io/solutions/responsive-landing-page-using-mobile-first-sass-Hy0lVH4Sc>

## Notes

My first full landing page. Mobile-first and working well for any screen sizes. Really tried to make it pixel-perfect to original designs, got really close to it in both mobile and desktop.

## Stuff I learned

- Used Figma for the first time, not with premium figma file, but with just free jpegs, rebuilt all layouts on top of jpegs with rectangles and text objects. Figma is awesome!
- Made a burger menu button via `--open` class toggling.
- Generally improved my understanding and skill of responsive design.